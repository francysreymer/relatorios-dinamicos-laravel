<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metas', function (Blueprint $table) {
            $table->id();
            $table->string('model');
            $table->json('columns');
            $table->boolean('active')->default(1);
            $table->timestamps();
            $table->engine = 'InnoDB';	
        });

        DB::table('metas')->insert([
            ['model' => 'websites', 
            'columns' => '[{"domain":{"type":"varchar(255)","translate":"Domínio"}, "active":{"type":"boolean","translate":"Ativo"}}]', 
            'active' => true,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metas');
    }
}
