<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('meta_id');
            $table->foreign('meta_id')->references('id')->on('metas');
            $table->string('name');
            $table->text('filter');
            $table->text('original_filter');
            $table->json('columns');
            $table->boolean('active')->default(1);
            $table->timestamps();
            $table->engine = 'InnoDB';	
        });

        DB::table('reports')->insert([
            ['meta_id' => 1, 
            'name' => 'Relatório Veus Teste 1', 
            'filter' => '(created_at >= "2021-00-00 00:00:00" AND created_at <= "2021-12-31 23:59:59" AND domain LIKE "%.com%") OR (domain REGEXP ".net$")',
            'original_filter' => '(Data da criação >= "01/01/2021" E Data da criação <= "31/12/2021" E domain Contém ".com") OU (Domínio Termina com ".net")',
            'columns' => '["domain", "active"]', 
            'active' => true,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
