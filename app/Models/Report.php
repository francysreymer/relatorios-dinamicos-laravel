<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'filter',
        'original_filter',
        'columns',
        'active',
        'meta_id',
    ];

    public function meta()
    {
        return $this->belongsTo(Meta::class);
    }

    protected $casts = ['columns' => 'array'];
}
