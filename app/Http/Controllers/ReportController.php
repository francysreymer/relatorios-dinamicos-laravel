<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Report;

class ReportController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = Report::with('meta')
                    ->where('id', $id)
                    ->where('active', true);

        if($query->exists()) {
            $report = $query->first();
            $meta = $report->meta()->first();
            $query = DB::table($meta->model)->whereRaw($report->filter)->where('active', true);
            foreach ($report->columns as $column) {
                $query->addSelect($column);
            }
            return view('show', ['report' => json_encode($query->get())]);
        } else {
            return view('show', ['report' => NULL]);
        }
    }
}
