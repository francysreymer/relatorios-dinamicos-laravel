<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReportTest extends TestCase
{
    /**
     * A basic feature test get report.
     *
     * @return void
     */
    public function test_get_reports()
    {
        $response = $this->call('GET', '/reports/1');
        $this->assertEquals(200, $response->status());
    }
}
